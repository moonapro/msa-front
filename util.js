const axios = require('axios');


global.__isDebugMode = true;		//console debug message 표시 여부

var util = {};

util.log = function(msg) {
	if(__isDebugMode) console.log(msg);
}

util.userData = {
	username: "",
	name: "",
	email: ""
}


util.setStep = function(rmnno, step, callback) {
	axios.post(__DASHBOARD_API_URI + "/insert", 
	{
		"rmnno": rmnno,
		"status_value": {
			"step": step
		}
	}	
	)
	.then((ret) => {
//		console.log(ret);
		callback(ret);
	})
	.catch((error) => {
		console.log(error);
		callback(error);
	});	
}



module.exports = util;