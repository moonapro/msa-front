/*!
 * modified by hklee 
 * SOURCE from
 * WebRTC Lab 
 * @author dodortus (codejs.co.kr / dodortus@gmail.com)
 *
 */

$(function() {
  if (typeof webkitSpeechRecognition !== 'function') {
//    alert('크롬에서만 동작 합니다.');
    return false;
  }

  let isRecognizing = false;
  let ignoreEndProcess = false;
  let finalTranscript = '';

  const recognition = new webkitSpeechRecognition();
  const language = 'ko-KR';
  const two_line = /\n\n/g;
  const one_line = /\n/g;
  const first_char = /\S/;

  recognition.continuous = true;
  recognition.interimResults = true;

  /**
   * 음성 인식 시작 처리
   */
  recognition.onstart = function() {
    console.log('onstart', arguments);
    isRecognizing = true;
  };

  /**
   * 음성 인식 종료 처리
   * @returns {boolean}
   */
  recognition.onend = function() {
    console.log('onend', arguments);
    isRecognizing = false;

    if (ignoreEndProcess) {
      return false;
    }

    // DO end process
    if (!finalTranscript) {
      console.log('empty finalTranscript');
      return false;
    }
  };

  /**
   * 음성 인식 결과 처리
   * @param event
   */
  recognition.onresult = function(event) {
    console.log('onresult', event);

    let interimTranscript = '';
    if (typeof(event.results) === 'undefined') {
      recognition.onend = null;
      recognition.stop();
      return;
    }

    for (let i = event.resultIndex; i < event.results.length; ++i) {
      if (event.results[i].isFinal) {
        finalTranscript += event.results[i][0].transcript;
      } else {
        interimTranscript += event.results[i][0].transcript;
      }
    }

    finalTranscript = capitalize(finalTranscript);

    console.log('인식결과 => ', interimTranscript);
    
    doVoiceCommand(interimTranscript);

  };

  /**
   * 음성 인식 에러 처리
   * @param event
   */
  recognition.onerror = function(event) {
    console.log('onerror', event);

    if (event.error.match(/no-speech|audio-capture|not-allowed/)) {
      ignoreEndProcess = true;
    }
  };

//음성인식 결과 처리: 사용하는 페이지에 구현
 /*
function doVoiceCommand(string) {
	  var cmd = string.replace(/ /gi, '');
	  
	  if (cmd.endsWith('상품추가')) {
		  
	  } else {
	  	  
	  }
}
*/
  
  /**
   * 개행 처리
   * @param s
   * @returns {string}
   */
  function linebreak(s) {
    return s.replace(two_line, '<p></p>').replace(one_line, '<br>');
  }

  /**
   * 첫문자를 대문자로 변환
   * @param s
   * @returns {string | void | *}
   */
  function capitalize(s) {
    return s.replace(first_char, function(m) {
      return m.toUpperCase();
    });
  }

  /**
   * 음성 인식 트리거
   * @param event
   */
  function start(event) {
    if (isRecognizing) {
      recognition.stop();
      return;
    }
    recognition.lang = language;
    recognition.start();
    ignoreEndProcess = false;

    finalTranscript = '';
  }

  start();
});

//TTS
/**
 * 문자를 음성으로 읽어 줍니다.
 * 지원: 크롬, 사파리, 오페라, 엣지
 */
function textToSpeech(text) {
  console.log('textToSpeech', arguments);

  // speechSynthesis option
  // const u = new SpeechSynthesisUtterance();
  // u.text = 'Hello world';
  // u.lang = 'en-US';
  // u.rate = 1.2;
  // u.onend = function(event) {
  //   log('Finished in ' + event.elapsedTime + ' seconds.');
  // };
  // speechSynthesis.speak(u);

  // simple version
  speechSynthesis.speak(new SpeechSynthesisUtterance(text));
}

//--- 문자열 처리 함수
function Left(str, n){
	if (n <= 0)
	  return "";
	else if (n > String(str).length)
	  return str;
	else
	  return String(str).substring(0,n);
}
function Right(str, n){
	  if (n <= 0)
	     return "";
	  else if (n > String(str).length)
	     return str;
	  else {
	     var iLen = String(str).length;
	     return String(str).substring(iLen, iLen - n);
	  }
}
