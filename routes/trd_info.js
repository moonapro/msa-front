/*jshint esversion: 6 */

//--- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
const util = require(__BASEDIR+'/util');
//-----------

//---- 기본 library 셋팅
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());
//--------------


//--- 금융거래 사전정보 표시
router.get("/trd_info/:rmnno", (req, res) => {
	util.log("금융거래 사전정보 화면");
    let body = req.body;

    axios.get(__BIZ_API_URI+"/api/trade/trade/advance_info", 
	{
		params: {
			Rmnno: req.params.rmnno
		}	
	})
	.then((ret) => {
		util.log("Success to get detail info !");
//		console.log(ret);
		
        body.a = ret.data.DomAddrIon;
        body.b = ret.data.DomCorpHldIon;
        body.c = ret.data.DomIdtyGrtrIon;
        body.d = ret.data.DomJobIon;
        body.e = ret.data.ScpstCnfIstmIon;
                    
		res.render("bizlogic/trd_info", {
			rmnno: req.params.rmnno,
			data: body
		});
	})
	.catch((error) => {
		if(error.response.status == 404 || error.response.status == 400) {
			// 등록정보 없을 경우 초기화
			body = {
				a: "",	b: "",	c: "",	d: "",	e: ""
			}

			res.render("bizlogic/trd_info", {
				rmnno: req.params.rmnno,
				data: body
			});
		}
		else{
			console.error("Fail to get info!", error);
			res.render("error", {
				message: "금융거래 사전정보 실패", 
				code: error.response.status,
				returnurl: "/rmnno"
			});
        }
    });
});
//--------------


//--- 금융거래 사전정보 로직
router.post("/trd_info", (req, res) => {
	util.log("금융거래 사전정보 Process");
	let body = req.body;
	
	axios.post(__BIZ_API_URI+"/api/trade/trade/advance_info", 
		{
			Rmnno: body.rmnno,
            TrdAdvInfPrm: {
                DomAddrIon: body.a,
                DomCorpHldIon: body.b,
                DomIdtyGrtrIon: body.c,
                DomJobIon: body.d,
                ScpstCnfIstmIon: body.e
            }
		}
	)
	.then((ret) => {
		if(ret.status == 200) {
			util.setStep(body.rmnno, "step4", function(ret) {

            res.render("error", {
				message: "고객정보가 정상적으로 저장되었습니다.", 
				code: 200,
				returnurl: "/rmnno"+""
			});
        })            
//			res.redirect("/rmnno/"+""); //계좌개설 화면으로...
		} else {
			res.redirect("/trd_info/"+body.rmnno);
		}
	})
	.catch((error) => {
		console.error(error);
        res.render("error", {
            message: "금융거래 사전정보 정보저장 실패, 입력정보를 확인해주세요.", 
            code: error.response.status,
            returnurl: "/trd_info/"+body.rmnno
        });
	});	
});
//----------------

module.exports = router;
