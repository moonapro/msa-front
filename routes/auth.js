/*jshint esversion: 6 */

//--- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
const util = require(__BASEDIR+'/util');
//-----------

//---- 기본 library 셋팅
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());
//--------------

router.get("/", (req, res) => {
	res.redirect("/rmnno");
});


//--- 실명확인번호 입력/검증화면 표시
router.get("/rmnno", (req, res) => {
	util.log("실명확인번호 입력/검증 화면");
	//res.cookie(__ACCESS_TOKEN_NAME, "");
	util.log("id:"+util.userData.username);
	
	res.render("login/rmnno");
});
//--------------


//--- 실명확인번호 입력/검증 로직
router.post("/rmnno2", (req, res) => {
	util.log("실명번호확인 Process====rmnno2");
	let body = req.body;
	util.log("rmnno:"+body.rmnno);

	axios.get(__BIZ_API_URI+"/api/name/info", 
	{
        params: {
            Rmnno: body.rmnno
        }	
 	})
    .then((ret) => {
		res.redirect("/prs_agr/"+body.rmnno);
	})
	.catch((error) => {
		console.error("Fail to get info!", error);

		res.render("error", {
			message: "미등록 실명번호입니다.", 
			code: error.response.status,
			returnurl: "/rmnno"
		});
	})
});


//--- 실명확인번호 입력/검증 로직
router.post("/rmnno", (req, res) => {
	util.log("실명번호확인 Process====");
	let body = req.body;
	util.log("rmnno:"+body.rmnno);
	axios.get(__DASHBOARD_API_URI + "/DashStatus", 
		{
			params: {
				rmnno: body.rmnno
			}
		}
	)
	.then((ret) => {
//		console.log(ret);
		if(ret.status == 200) {
			let data = ret.data.value.status_value;
			let obj = JSON.parse(data);
			let step = obj.step;
			util.log(data+"=>"+step);
			switch(step){
				case "step0" : res.redirect("/prs_agr/"+body.rmnno); break;
				case "step1" : res.redirect("/cust_info/"+body.rmnno); break;
				case "step2" : res.redirect("/trd_agr/"+body.rmnno); break;
				case "step3" : res.redirect("/trd_info/"+body.rmnno); break;
				case "step4" : res.render("error", {
									message: "이미 고객정보 등록이 완료된 고객입니다.", 
									code: 200,
									returnurl: "/rmnno"+""
								});
			}			  
			
//			res.redirect("/prs_agr/"+body.rmnno);
		} else {
			goFirst();
		}
	})
	.catch((error) => {
		goFirst();
	})

	let goFirst = function() {
		axios.post(__BIZ_API_URI + "/api/name/info", 
		{
			Rmnno: body.rmnno
		}
		)
		.then((ret) => {
			if(ret.status == 200) {
				res.redirect("/prs_agr/"+body.rmnno);
			} else {
				res.redirect("/rmnno");
			}
		})
		.catch((error) => {
		//		console.error(error);
		//		res.redirect("/rmnno");
		//		util.log("body.rmnno=>"+body.rmnno);
		//res.redirect("/prs_agr/"+body.rmnno);
		res.render("error", {
			message: "실명번호 OOOOOO-OOOOOOO 형식으로 입력하세요.", 
			code: error.response.status,
			returnurl: "/rmnno"
		});

		});	
	}


	
});
//--------------------------------------------------------------------------------------------------------------



//--- Login화면 표시
router.get("/login", (req, res) => {
	util.log("Login 화면");
	res.cookie(__ACCESS_TOKEN_NAME, "");
	res.render("login/login");
});
//--------------

//--- 인증처리 로직
router.post("/login", (req, res) => {
	util.log("Login Process");
	let body = req.body;
	
	axios.post(__AUTH_API_URI+"/api/auth/login", 
		{
			username: body.username,
			password: body.password			
		}
	)
	.then((ret) => {
		if(ret.status == 200) {
			util.log("##### Gerated Access Token=>"+ret.data.data);
			res.cookie(__ACCESS_TOKEN_NAME, ret.data.data);		//cookie에 임시로 저장
			res.redirect("/rmnno");
		} else {
			res.redirect("/login");
		}
	})
	.catch((error) => {
		console.error(error);
		res.redirect("/login");
	});	
});
//----------------

//--- 회원가입 화면 rendering
router.get("/signup", (req, res) => {
	util.log("회원가입 화면");
	res.render("login/signup", { mode: "get" });
});
//---------

//--- 회원가입 처리 
router.post("/signup", (req, res) => {
	util.log("회원가입 저장");
	let body = req.body;

	axios.post(__AUTH_API_URI+"/api/users", 
		{
			username: body.username,
			password: body.password	,
			passwordConfirmation: body.passwordConfirmation,
			name: body.name,
			email: body.email
		}
	)
	.then((ret) => {
		//util.log(ret);
		if(ret.status == 200) {
			let retData = {
				success: "",
				msg: ""
			}
			if(ret.data.success) {
				retData.success =  true;			
			} else {
				util.log("** ERROR=>"+ret.data.errors);	
				retData.success =  false;
				retData.msg = JSON.parse(ret.data.errors);
			}
			res.render("login/signup", { mode: "post", postData: body, result: retData });
		} else {
			res.redirect("/signup");
		}
	})
	.catch((error) => {
		console.error(error);
		res.redirect("/signup");
	});	
});
//--------------

//---- Logout
router.get("/logout", (req, res) => {
	util.log("Logout 화면");
	
	res.redirect("/login");
});
//-----------

module.exports = router;