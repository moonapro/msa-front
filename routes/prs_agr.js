/*jshint esversion: 6 */

//--- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
const util = require(__BASEDIR+'/util');
//-----------

//---- 기본 library 셋팅
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());
//--------------


//--- 실명확인번호 입력/검증화면 표시
router.get("/prs_agr/:rmnno", (req, res) => {
	util.log("개인정보 수집/이용 동의 화면");
	let body = req.body;

	util.log("실명번호 = "+req.params.rmnno);

	axios.get(__BIZ_API_URI+"/api/customer/usage_agree", 
	{		
		//headers: body.rmnno,
		params: {
			Rmnno: req.params.rmnno
		}	
	})
	.then((ret) => {
		util.log("개인정보 수집/이용 동의정보 수신성공 !");
//		console.log(ret);
		
		body.a = ret.data.PsnlCtinfoColltAgrmYn;
		body.b = ret.data.IdfyInfoTrxAgrmYn;
		body.c = ret.data.AgntInfoColltAgrmYn;
		body.d = ret.data.PsnlInfoMktgAgrmYn;
		body.e = ret.data.MgntrnPrdtAgrmYn;
		body.f = ret.data.NTaxchrIntgSavAgrmYn;
		body.g = ret.data.PsantAgrmYn;
		body.h = ret.data.InfoTrxAgrmYn;
		
		res.render("bizlogic/prs_agr", {
			rmnno: req.params.rmnno,
			data: body
		});
	})
	.catch((error) => {
		if(error.response.status == 404 || error.response.status == 400) {
			// 등록정보 없을 경우 초기화
			body = {
				a: " ",	b: " ",	c: " ",	d: " ",	e: " ",	f: " ",	g: " ",	h: " "
			}

			res.render("bizlogic/prs_agr", {
				rmnno: req.params.rmnno,
				data: body
			});
		}
		else{
			console.error("Fail to get info!", error);
			res.render("error", {
				message: "개인정보 수집/이용 동의 정보조회 실패", 
				code: error.response.status,
				returnurl: "/rmnno"
			});
		}
	});	
});
//--------------


//--- 실명확인번호 입력/검증 로직
router.post("/prs_agr_post", (req, res) => {
	util.log("개인정보 수집/이용 동의 Process POST");
	let body = req.body;

	console.log("body.rmnno = "+body.rmnno);

	axios.post(__BIZ_API_URI+"/api/customer/usage_agree", 
	{
		ClntUsgAgrPrm: {
			PsnlCtinfoColltAgrmYn: body.a,
			IdfyInfoTrxAgrmYn: body.b,			
			AgntInfoColltAgrmYn: body.c,
			PsnlInfoMktgAgrmYn: body.d,			
			MgntrnPrdtAgrmYn: body.e,
			NTaxchrIntgSavAgrmYn: body.f,			
			PsantAgrmYn: body.g,
			InfoTrxAgrmYn: body.h
		},	
		Rmnno: body.rmnno
	})
	.then((ret) => {
		if(ret.status == 200) {
			util.setStep(body.rmnno, "step1", function(ret) {
				res.redirect("/cust_info/"+body.rmnno);
			})

//			res.redirect("/cust_info/"+body.rmnno);
		} else {
			res.redirect("/prs_agr/"+body.rmnno);
		}
	})
	.catch((error) => {
		console.error(error);	
		res.render("error", {
			message: "개인정보 수집/이용 동의 정보저장 실패", 
			code: error.response.status,
			returnurl: "/rmnno"
		});
		res.redirect("/prs_agr/"+body.rmnno);
	});	
});
//----------------

module.exports = router;
