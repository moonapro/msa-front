/*jshint esversion: 6 */

//--- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
const util = require(__BASEDIR+'/util');
//-----------

//---- 기본 library 셋팅
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());
//--------------


//--- 금융거래 목적확인화면 표시
router.get("/trd_agr/:rmnno", (req, res) => {
	util.log("금융거래 목적확인 화면");
    let body = req.body;

    axios.get(__BIZ_API_URI+"/api/trade/trade/purpose_check", 
	{
		params: {
			Rmnno: req.params.rmnno
		}	
	})
	.then((ret) => {
		util.log("Success to get detail info !");
//		console.log(ret);
		
		body.a = ret.data.FntdPurpsCnfDmndYn;
		body.b = ret.data.OthrPsnPsbLndReqstRealYn;
		body.c = ret.data.OthrInPsbOpnReqstRealYn;
		body.d = ret.data.CorpPsbOpnRealYn;
				
		res.render("bizlogic/trd_agr", {
			rmnno: req.params.rmnno,
			data: body
		});
	})
	.catch((error) => {
		if(error.response.status == 404 || error.response.status == 400) {
			// 등록정보 없을 경우 초기화
			body = {
				a: "",	b: "",	c: "",	d: ""
			}

			res.render("bizlogic/trd_agr", {
				rmnno: req.params.rmnno,
				data: body
			});
		}
		else{
			console.error("Fail to get info!", error);
			res.render("error", {
				message: "금융거래 목적확인 실패", 
				code: error.response.status,
				returnurl: "/rmnno"
			});
		}

	});	

});
//--------------


//--- 금융거래 목적확인 로직
router.post("/trd_agr", (req, res) => {
	util.log("금융거래 목적확인 Process");
	let body = req.body;
	
	axios.post(__BIZ_API_URI+"/api/trade/trade/purpose_check", 
	{

        Rmnno: body.rmnno,
        TrdPrpCnfPrm: {
            FntdPurpsCnfDmndYn: body.a,
            OthrPsnPsbLndReqstRealYn: body.b,
            OthrInPsbOpnReqstRealYn: body.c,
            CorpPsbOpnRealYn: body.d
        }		
    },
    {
        headers: {
            'Content-Type': 'application/json'
        }
    })
	.then((ret) => {
		if(ret.status == 200) {
            util.setStep(body.rmnno, "step3", function(ret) {
				res.redirect("/trd_info/"+body.rmnno);
			})
	
//			res.redirect("/trd_info/"+body.rmnno);
		} else {
			res.redirect("/trd_agr/"+body.rmnno);
		};
	})
	.catch((error) => {
//console.error("Errorcode="+error.response.status);        
console.error(error);        
    res.render("error", {
            message: "금융거래 목적확인 정보저장 실패, 입력정보를 확인해주세요.", 
            code: error.response.status,
            returnurl: "/trd_agr/"+body.rmnno
        });
//res.redirect("/trd_agr/"+body.rmnno);
	});	
});
//----------------

module.exports = router;
