/*jshint esversion: 6 */

//--- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
const util = require(__BASEDIR+'/util');
//-----------

//---- 기본 library 셋팅
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());
//--------------


//--- 기본정보입력화면 표시
router.get("/cust_info/:rmnno", (req, res) => {
    util.log("기본정보입력화면 화면");
    let body = req.body;

	axios.get(__BIZ_API_URI+"/api/customer/person_info", 
	{
        params: {
            Rmnno: req.params.rmnno
        }	
 	})
    .then((ret) => {
		util.log("Success to get detail info !"+ret.data.ClntNm);
		
		body.clnt_nm = ret.data.ClntNm;
		body.ntlt_nm = ret.data.NtltNm;
		body.frgr_yn = ret.data.FrgrYn;
		body.rsdn_yn = ret.data.RsdnYn;
		body.addr = ret.data.Addr;
		body.email = ret.data.Email;
        body.tel_no = ret.data.TelNo;
        
        res.render("bizlogic/cust_info", {
            rmnno: req.params.rmnno,
            data: body   
        });
    })
	.catch((error) => {
 		if(error.response.status == 404 || error.response.status == 400) {
			// 등록정보 없을 경우 초기화
			body = {
                clnt_nm: "",	
                ntlt_nm: "",	
                frgr_yn: "",	
                rsdn_yn: "",	
                addr: "",	
                email: "",	
                tel_no: ""             
			}

			res.render("bizlogic/cust_info", {
				rmnno: req.params.rmnno,
				data: body
			});
		}
		else{
			console.error("Fail to get info!", error);
			res.render("error", {
				message: "기본정보 조회 실패", 
				code: error.response.status,
				returnurl: "/rmnno"
			});
		}

	});	    
});
//--------------


//--- 기본정보입력 로직
router.post("/cust_info_post", (req, res) => {
	util.log("기본정보입력 Process");
	let body = req.body;
	
	axios.post(__BIZ_API_URI+"/api/customer/person_info", 
		{
            ClntPrm: {
                ClntNm: body.clnt_nm,
                NtltNm: body.ntlt_nm,
                FrgrYn: body.frgr_yn,
                RsdnYn: body.rsdn_yn,
                Addr: body.addr,
                Email: body.email,
                TelNo: body.tel_no
            },

            Rmnno: body.rmnno
 		}
	)
	.then((ret) => {
		if(ret.status == 200) {
            util.setStep(body.rmnno, "step2", function(ret) {
				res.redirect("/trd_agr/"+body.rmnno);
			})

//			res.redirect("/trd_agr/"+body.rmnno);
		} else {
			res.redirect("/cust_info/"+body.rmnno);
		}
	})
	.catch((error) => {
		console.error(error);
		res.render("error", {
            message: "기본정보입력저장 실패, 입력정보를 확인해주세요.", 
            code: error.response.status,
            returnurl: "/cust_info/"+body.rmnno
        });
	});	
});
//----------------

module.exports = router;
